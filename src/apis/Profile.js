import http from '../http-common';

class Profile{
    get(){
        return http.get("/person");
    }

    update(data){
        return http.put("/person", data, {
            headers: {
                "Content-Type": "multipart/form-data"
            },
        });
    }

    getLang(){
        return http.get("/person/language");
    }

    createLang(data){
        return http.post("/person/language", data);
    }
    
    updateLang(id, data){
        return http.put(`/person/language/${id}`, data);
    }

    deleteLang(id){
        return http.delete(`/person/language/${id}`);
    }

    generateDocx(){
        return http.get("/person/generate");
    }
}

export default new Profile();