import http from '../http-common';

class Course{
    get(){
        return http.get("/course");
    }

    create(data){
        return http.post("/course", data);
    }

    getById(id){
        return http.get(`/course/${id}`);
    }

    update(id, data){
        return http.put(`/course/${id}`, data);
    }

    delete(id){
        return http.delete(`/course/${id}`);
    }
}

export default new Course();