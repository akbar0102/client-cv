import http from '../http-common';

class Education{
    getEducation(){
        return http.get("/education");
    }

    createEdu(data){
        return http.post("/education", data);
    }

    getOneEdu(id){
        return http.get(`/education/${id}`);
    }

    updateEdu(id, data){
        return http.put(`/education/${id}`, data);
    }

    deleteEdu(id){
        return http.delete(`/education/${id}`);
    }
}

export default new Education();