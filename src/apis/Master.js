import http from '../http-common';

class Master{
    getDegree(){
        return http.get("/master/degree");
    }

    getReligion(){
        return http.get("/master/religion");
    }

    getJob(){
        return http.get("/master/job");
    }
    
}

export default new Master();