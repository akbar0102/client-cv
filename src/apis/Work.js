import http from '../http-common';

class Work{
    get(){
        return http.get("/work");
    }

    create(data){
        return http.post("/work", data);
    }

    getById(id){
        return http.get(`/work/${id}`);
    }

    update(id, data){
        return http.put(`/work/${id}`, data);
    }

    delete(id){
        return http.delete(`/work/${id}`);
    }

    getRole(id_pengalaman_kerja){
        return http.get(`/work/roles/${id_pengalaman_kerja}`);
    }

    createRole(id_pengalaman_kerja, data){
        return http.post(`/work/role/${id_pengalaman_kerja}`, data);
    }

    deleteRole(id_role){
        return http.delete(`/work/role/${id_role}`);
    }
}

export default new Work();