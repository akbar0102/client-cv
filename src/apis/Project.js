import http from '../http-common';

class Project{
    get(){
        return http.get("/project");
    }

    create(data){
        return http.post("/project", data);
    }

    getById(id){
        return http.get(`/project/${id}`);
    }

    update(id, data){
        return http.put(`/project/${id}`, data);
    }

    delete(id){
        return http.delete(`/project/${id}`);
    }

    getRole(id_project){
        return http.get(`/project/roles/${id_project}`);
    }

    createRole(id_project, data){
        return http.post(`/project/role/${id_project}`, data);
    }

    deleteRole(id){
        return http.delete(`/project/role/${id}`);
    }
}

export default new Project();