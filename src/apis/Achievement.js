import http from '../http-common';

class Achievement{
    get(){
        return http.get("/achievement");
    }

    getById(id){
        return http.get(`/achievement/${id}`);
    }

    create(data){
        return http.post("/achievement", data);
    }

    update(id, data){
        return http.put(`/achievement/${id}`, data);
    }

    delete(id){
        return http.delete(`/achievement/${id}`);
    }
}

export default new Achievement();