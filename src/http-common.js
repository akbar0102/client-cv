import axios from 'axios';

// export default axios.create({
//     baseURL: "http://localhost:3000/api/cv",
//     headers: {
//         "auth-token": localStorage.getItem('token'),
//         "Content-type": "application/json"
//     }
// });

const ajax = axios.create({
    baseURL: "http://localhost:3000/api/cv"
});

ajax.interceptors.request.use(
    (config) => {
      let token = localStorage.getItem('token')
  
      if (token) {
        config.headers['auth-token'] = `${ token }`
      }
  
      return config
    },
  
    (error) => {
      return Promise.reject(error)
    }
  )
  
  export default ajax